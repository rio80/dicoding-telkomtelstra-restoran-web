<?php
if (isset($_SERVER['REQUEST_METHOD']) == 'GET') {
    require_once 'koneksi.php';

    $searchText = isset($_GET['search']) ? $_GET['search'] : "";
	$jenis    = $_GET['jenis'];

    $response = array();
    $data     = array();

    $query = "";
    $query = "SELECT
                idMenu as id, nama, harga, CONCAT(server_addr,path_img) as full_path
                FROM menu WHERE jenis LIKE '%$jenis%' AND
                (nama LIKE '%$searchText%' OR harga LIKE '%$searchText%'); ";

    $exec = mysqli_query($con, $query);

    $cek = mysqli_num_rows($exec);

    if ($cek > 0) {
        while ($row = mysqli_fetch_assoc($exec)) {
            $data[] = $row;
        }
        $response['value']  = '1';
        $response['result'] = $data;
    } else {
        $response['value']  = '0';
        $response['result'] = $data;
    }
    $json_encode = json_encode($response);

    echo $json_encode;
}
