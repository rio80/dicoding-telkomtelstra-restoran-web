<?php
if (isset($_SERVER['REQUEST_METHOD']) == 'POST') {
    require_once 'koneksi.php';
    $koneksi    = $con;
    $idOrder    = "";
    $idOrderDtl = "";

    $total    = 0;
    $response = array();
    $idCust   = isset($_POST['idCust']) ? $_POST['idCust'] : "";
    $idMenu   = isset($_POST['idMenu']) ? $_POST['idMenu'] : "";
    $idOrder  = isset($_POST['idOrder']) ? $_POST['idOrder'] : "";
    $jml      = isset($_POST['jml']) ? $_POST['jml'] : 0;
    $harga    = isset($_POST['harga']) ? $_POST['harga'] : 0;

    $query1 = " SELECT * FROM order_master LIMIT 1;";
    $query2 = " SELECT * FROM order_detail LIMIT 1;";

    $cekData1 = mysqli_num_rows(mysqli_query($koneksi, $query1));
    $cekData2 = mysqli_num_rows(mysqli_query($koneksi, $query2));

    // die($cekData1.'-'.$cekData2);

    if ($cekData1 < 1 ||
        $cekData2 < 1) {

        $idOrder    = "TM000001";
        $idOrderDtl = "TD000001";

        //Kerjakan perintah Insert ke ORDER dan ORDER_DETAIL
        insertOrderDtl($koneksi, $idOrderDtl, $idCust, $idMenu, $idOrder, $jml, $harga);
        $total = totalOrder($koneksi, $jml, $idOrder, $harga);
        insertOrder($koneksi, $idOrder, $total);
        $response['value']   = '1';
        $response['message'] = 'Simpan Transaksi Baru Berhasil';
    } else {

        // Cek ID order dan detail terakhir, lalu Increment kan
        try {
            $idOrderDtl = cek_order_detail($koneksi);

            // die($idOrder.'-'.$idOrderDtl);
            // $idOrderLast=cek_last_id($con);
            insertOrderDtl($koneksi, $idOrderDtl, $idCust, $idMenu, $idOrder, $jml, $harga);
            $total = totalOrder($koneksi, $jml, $idOrder, $harga);

            insertOrder($koneksi, $idOrder, $total);

            $response['value']   = '1';
            $response['message'] = 'Simpan Transaksi Berhasil';
        } catch (Exception $e) {
            $response['value']   = '0';
            $response['message'] = 'Error Proses Simpan : \n'.$e->getMessage();
        }

    }

    echo json_encode($response);


}

function cek_last_id($con)
{
    $query = "";
    $query = "SELECT max(substr(idOrder,3)) as last_id,
    substr(idOrder,1,2) as kode
    FROM order_master ;";

    $cekExec = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($cekExec)) {

        $last_id = $row['kode'] . $row['last_id'];
    }

    return $last_id;
}

function cek_order_detail($con)
{
    $query = "";
    $query = "SELECT max(substr(idtransorder,3)+1) as newId,
                length(substr(idtransorder,3)) as lenId, idtransorder FROM order_detail ; ";

    $cekExec      = mysqli_query($con, $query);
    $newId        = 0; // Untuk mendapatkan nomor ID yg baru
    $idtransorder = ''; // Untuk mendapatkan ID yang lama
    $hasil        = array();
    while ($row = mysqli_fetch_array($cekExec)) {

        $newId        = $row['newId'];
        $idtransorder = $row['idtransorder'];
    }

    /* ---- Munculkan ID Order dengan di kurangi jumlah
    panjang dari jumlah yg sudah terdaftar--- */
    $idtransorder = substr($idtransorder, 0, (
        strlen(substr($idtransorder, 0)) - strlen($newId)
    ));

    // Lalu gabungkan IdCust yang sudah di substr dgn ID yang sudah di increment
    $idtransorder = $idtransorder . $newId;

    return $idtransorder;
}

function totalOrder($con, $jml, $idOrder, $harga)
{

    $query = "";
    $query = "SELECT SUM($jml * $harga) AS total
    			FROM order_detail WHERE idOrder='$idOrder' GROUP BY idOrder;";

     // die($query);

    $cekExec = mysqli_query($con, $query);
    $total   = 0; // Untuk mendapatkan nomor ID yg baru
    $hasil   = array();
    while ($row = mysqli_fetch_array($cekExec)) {

        $total = $row['total'];
    }
    return $total;
}

function insertOrder($con, $idOrder, $total)
{
    $query = "";
    $query = "SELECT idOrder FROM order_master WHERE idOrder='$idOrder'";

    $hasil   = mysqli_query($con, $query);
    $cekData = mysqli_num_rows($hasil);

    // Jika data pada order_master belum ada maka Buat data Baru
    if ($cekData < 1) {

        $query = "";
        $query = "INSERT INTO order_master( idOrder , total ) VALUES
			('$idOrder' , $total )";

        $hasil = mysqli_query($con, $query);
    } else {
        // Jika data pada order_master sudah ada maka update total pesan
        $query = "";
        $query = "UPDATE order_master SET total=$total WHERE idOrder='$idOrder';";

        $hasil = mysqli_query($con, $query);
    }

}

function insertOrderDtl($con, $idOrderDtl, $idCust, $idMenu, $idOrder, $jml, $harga)
{
    $query = "";
    $query = "INSERT INTO order_detail VALUES
			('$idOrderDtl' , '$idCust' , '$idMenu', '$idOrder' , $jml, $harga )";

    $hasil = mysqli_query($con, $query);

}
