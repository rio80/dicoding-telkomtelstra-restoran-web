<?php
if (isset($_SERVER['REQUEST_METHOD']) == 'GET') {
    require_once 'koneksi.php';

    $jenis    = $_GET['jenis'];
    $query    = '';
    $response = array();
    $data     = array();
    $query    = "SELECT
                idMenu as id, nama, harga, CONCAT(server_addr,path_img) as full_path
                FROM menu WHERE jenis LIKE '%$jenis%';";

    
    $exec = mysqli_query($con, $query);

    $cek = mysqli_num_rows($exec);

    if ($cek > 0) {
        while ($row = mysqli_fetch_assoc($exec)) {
            $data[] = $row;
        }
        $response['value']  = '1';
        $response['result'] = $data;
    } else {
        $response['value']  = '0';
        $response['result'] = $data;
    }

    $json_encode = json_encode($response);

    echo $json_encode;
}
