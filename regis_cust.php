<?php

/* Disini U/ Registrasi Cust sekaligus
dengan Insert ke Tabel Login */
require_once 'koneksi.php';
if (isset($_SERVER['REQUEST_METHOD']) == 'POST') {
    $koneksi  = $con;
    $id_cust  = '';
    $id_login = '';
    $nama     = isset($_POST['nama']) ? $_POST['nama'] : "-";
    $alamat   = isset($_POST['alamat']) ? $_POST['alamat'] : "-";
    $jk       = isset($_POST['jk']) ? $_POST['jk'] : "-";
    $notelp   = isset($_POST['notelp']) ? $_POST['notelp'] : "-";

    $jenis    = isset($_POST['jenis']) ? $_POST['jenis'] : "-";
    $username = isset($_POST['username']) ? $_POST['username'] : "-";
    $password = isset($_POST['password']) ? $_POST['password'] : "-";

    $query = "SELECT idCust FROM cust limit 1";

    // Untuk menampung hasil query apakah ada Data atau tidak
    $cekExec = mysqli_num_rows(mysqli_query($koneksi, $query));

    if ($cekExec < 1) {
        // jika cekExec < 1 / tidak ada data maka jalankan perintah berikut
        $id_cust  = 'C0000001';
        $id_login = 'L0000001';

        insertCust($koneksi, $id_cust, $nama, $alamat, $jk, $notelp,
            $id_login, $jenis, $username, $password);

    } else {

        $id_cust  = cek_Cust($koneksi);
        $id_login = cek_Login($koneksi);

        // Lakukan pengecekan apabila Username dan password sudah terdaftar
        // Maka tidak bisa Input Cust dan Login
        if (!cek_user_login($koneksi, $username, $notelp)) {
            // Simpan Data
            insertCust($koneksi, $id_cust, $nama, $alamat, $jk, $notelp,
                $id_login, $jenis, $username, $password);
        } else {
            $response = array();

            $response['value']   = '2';
            $response['message'] = 'User dan password sudah terdaftar';

            echo json_encode($response);
        }

    }
}
// /
// /
//
//
// /
// /
//
//
function cek_Cust($con)
{
    $query = "";
    $query = "SELECT max(substr(idCust,2)+1) as newId,
                length(substr(idcust,2)) as lenId, idCust FROM cust ; ";
    $cekExec = mysqli_query($con, $query);
    $newId   = 0; // Untuk mendapatkan nomor ID yg baru
    $idCust  = ''; // Untuk mendapatkan ID yang lama
    $hasil   = array();
    while ($row = mysqli_fetch_array($cekExec)) {
        $newId  = $row['newId'];
        $idCust = $row['idCust'];
    }

    /* ---- Munculkan ID Cust dengan di kurangi jumlah
    panjang dari jumlah yg sudah terdaftar--- */
    $idCust = substr($idCust, 0, (
        strlen(substr($idCust, 0)) - strlen($newId)
    ));

    // Lalu gabungkan IdCust yang sudah di substr dgn ID yang sudah di increment
    $idCust = $idCust . $newId;

    return $idCust;
}
// /
// /
//
//
// /
// /
//
//
function cek_user_login($con, $username, $notelp)
{
    $query = "";
    $query = "  SELECT username, password,id_type FROM login
                inner join cust as c On login.id_type=c.idCust
                WHERE username='$username' OR notelp='$notelp';";
    
    $exec  = mysqli_query($con, $query);
    $hasil = mysqli_num_rows($exec);

    if ($hasil > 0) {
        return true;
    } else {
        return false;
    }

}

function cek_Login($con)
{
    $query = "";
    $query = "SELECT max(substr(id_login,2)+1) as newId,
            length(substr(id_login,2)) as lenId, id_login
            FROM login ; ";
    $cekExec  = mysqli_query($con, $query);
    $newId    = 0; // Untuk mendapatkan nomor ID yg baru
    $id_login = ''; // Untuk mendapatkan ID yang lama
    $hasil    = array();
    while ($row = mysqli_fetch_array($cekExec)) {
        $newId    = $row['newId'];
        $id_login = $row['id_login'];
    }

    /* ---- Munculkan ID Cust dengan di kurangi jumlah
    panjang dari jumlah yg sudah terdaftar--- */
    $id_login = substr($id_login, 0, (
        strlen(substr($id_login, 0)) - strlen($newId)
    ));

    $id_login = $id_login . $newId;

    return $id_login;
}
// /
// /
//
//
// /
// /
//
//
function insertCust($con, $idCust, $nama, $alamat, $jk, $notelp,
    $idlogin, $jenis, $username, $password) {

    $query = "";
    $query = " INSERT INTO cust VALUES('$idCust','$nama','$alamat','$jk','$notelp'); ";
    $query .= " INSERT INTO login (id_login,jenis,username,password,id_type)
                VALUES('$idlogin','$jenis','$username','$password', '$idCust'); ";

    // Menggunakan mysqli_multi_query untuk insert lebih dari 1 query
    $exec = mysqli_multi_query($con, $query);

    $response = array();

    if ($exec) {
        $response['value']   = '1';
        $response['message'] = 'Data berhasil disimpan!';
    } else {
        $response['value']   = '0';
        $response['message'] = 'Data Gagal disimpan!';
    }

    echo json_encode($response);
}
