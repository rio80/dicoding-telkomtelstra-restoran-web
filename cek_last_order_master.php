<?php
if (isset($_SERVER['REQUEST_METHOD']) == 'POST') {
    require_once 'koneksi.php';
    $query  = "";
    $idCust = isset($_POST['idCust']) ? $_POST['idCust'] : "";
    $query  = "SELECT order_master.idOrder,
            order_master.status_pesan,
            order_detail.idCust
            FROM order_detail
            INNER JOIN order_master ON order_master.idOrder=order_detail.idOrder
            WHERE order_detail.idCust='$idCust'
            Group by order_detail.idCust;";

    $status_pesan = 0;
    $newId        = 0; // Untuk mendapatkan nomor ID yg baru
    $idOrder      = ''; // Untuk mendapatkan ID yang lama
    $status_pesan = '';
    $hasil        = array();
    $exec         = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($exec)) {
        $status_pesan = $row['status_pesan'];
    }

    if ($status_pesan == 0) {
        $query = "";
        $query = "SELECT max(substr(idOrder,3)) as newId,
                status_pesan, idOrder FROM order_master ;";

    } else if ($status_pesan == 2) {
        $query = "";
        $query = "SELECT max(substr(idOrder,3)+1) as newId,
                $status_pesan as status_pesan, idOrder FROM order_master ; ";
    } else {
        $query = "";
        $query = "SELECT max(substr(idOrder,3)) as newId,
                $status_pesan as status_pesan, idOrder FROM order_master ; ";
    }
    $cekExec = mysqli_query($con, $query);

    while ($row = mysqli_fetch_array($cekExec)) {

        $newId        = $row['newId'];
        $idOrder      = $row['idOrder'];
        $status_pesan = $row['status_pesan'];
    }

    /* ---- Munculkan ID Order dengan di kurangi jumlah
    panjang dari jumlah yg sudah terdaftar--- */
    $idOrder = substr($idOrder, 0, (
        strlen(substr($idOrder, 0)) - strlen($newId)
    ));

    // Lalu gabungkan IdCust yang sudah di substr dgn ID yang sudah di increment
    $idOrder = $idOrder . $newId;

    if ($status_pesan == null) {
        $idOrder      = 'TM000001';
        $status_pesan = 0;
    }
     if ($status_pesan == 2) {
        $status_pesan = 0;
    }
    $hasil = array('last_order' => $idOrder, 'status_order' => $status_pesan);

    echo json_encode($hasil);
}
